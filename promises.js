const posts = [
		{
			title: "Post 1",
			body: "This is post 1"
		},
		{
			title: "Post 2",
			body: "This is post 2"
		},
		{
			title: "Post 3",
			body: "This is post 3"
		},
]

const getPosts = () => {
	setTimeout(() => {

		let output = "";

		posts.forEach((post) => {
			output += `<li>${post.title}</li>`
		})

		document.body.innerHTML = output;

	}, 1000)
}

const createPost = (post) => {

	return new Promise((resolve, reject) => {
		setTimeout(() => {
			posts.push(post)
			console.log("New post created")

			let error = true

			if(error){
				reject(new Error("Oops!"))
			}else{
				resolve()
			}
		}, 2000)
	})
}

const saveUserInfo = () => {

	return new Promise((resolve, reject) => {
		setTimeout(() => {
			console.log("New user info saved")

			let error = true

			if(error){
				reject(new Error("Oops!"))
			}else{
				resolve()
			}
		}, 1000)
	})
}

// createPost({title: "Post 4", body: "This is post 4"})
// .then(saveUserInfo)
// .then(getPosts)
// .catch(err => alert(err))

async function theBestWay(){
	try{
		await createPost({title: "Post 4", body: "This is post 4"})

		await saveUserInfo()

		getPosts()
	}catch(err){
		alert(err)
	}
}

theBestWay()