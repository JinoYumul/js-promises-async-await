const posts = [
		{
			title: "Post 1",
			body: "This is post 1"
		},
		{
			title: "Post 2",
			body: "This is post 2"
		},
		{
			title: "Post 3",
			body: "This is post 3"
		},
]

const getPosts = () => {
	setTimeout(() => {

		let output = "";

		posts.forEach((post) => {
			output += `<li>${post.title}</li>`
		})

		document.body.innerHTML = output;

	}, 1000)
}

const createPost = (post) => {
	setTimeout(() => {

	posts.push(post)
	console.log("New post created.")

	}, 2000)	
}

const saveUserInfo = (callback) => {
	setTimeout(() => {

	console.log("New user info saved")
	callback()

	}, 2000)
}

createPost({title: "Post 4", body: "This is post 4"}, saveUserInfo(getPosts))